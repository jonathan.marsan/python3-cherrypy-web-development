## Web Development Examples with Python and CherryPy

### Setup Notes

This project uses Python 3: `brew install python3`

#### Setting Up the Project

* Install virtualenv on the system `pip3 install virtualenv`
* Within project directory, create a virtual environment: `virtualenv -p python3 venv`
* Install required Python packages: `pip install -r requirements.txt`
* Install node: `brew install node`
* Activate the virtual environment `source venv/bin/activate`
* Install required JS packages: `npm install`
